#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
//! Minimal bitflags type implementation allowing to check if a set of flags
//! contains a specific flag. The type must implement Copy and Bitand with u32.
use core::ops::BitAnd;

pub trait BitFlags<T> {
    fn contains(&self, rhs: T) -> bool;
}

impl<T, U> BitFlags<T> for U
where
    U: BitAnd<T> + Copy,
    T: BitAnd<U>,
    <U as BitAnd<T>>::Output: PartialEq<u32>,
{
    fn contains(&self, rhs: T) -> bool {
        *self & rhs != 0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Clone, Copy)]
    enum Flags {
        A = 0b1,
        B = 0b10,
        C = 0b100,
    }

    impl BitAnd<Flags> for u32 {
        type Output = u32;

        fn bitand(self, rhs: Flags) -> Self::Output {
            self & rhs as u32
        }
    }

    impl BitAnd<u32> for Flags {
        type Output = u32;

        fn bitand(self, rhs: u32) -> Self::Output {
            self as u32 & rhs
        }
    }

    impl BitAnd for Flags {
        type Output = u32;

        fn bitand(self, rhs: Self) -> Self::Output {
            self as u32 & rhs as u32
        }
    }

    #[test]
    fn contains() {
        let num = 0b101;
        assert!(num.contains(Flags::A));
        assert!(!num.contains(Flags::B));
        assert!(num.contains(Flags::C));
    }
}
