use super::Cmd;
use clap::Command;

#[derive(Debug)]
pub struct Clear;

impl Cmd for Clear {
    fn cli(&self) -> Command {
        Command::new("clear")
            .about("clear the terminal's screen")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        print!("\x1b[2J\x1b[H");
        print!("\x1b[3J\x1b[H");
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
