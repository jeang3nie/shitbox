use shitbox::Cmd;

mod blkid;
mod bootstrap;
mod clear;
mod mount;
mod mountpoint;
mod swaplabel;
mod swapoff;
mod swapon;
mod umount;
mod utilbox;

/// Parses a string into a command to run
#[must_use]
pub fn get(name: &str) -> Option<Box<dyn Cmd>> {
    match name {
        "bootstrap" => Some(Box::new(bootstrap::Bootstrap)),
        "clear" => Some(Box::new(clear::Clear)),
        "mountpoint" => Some(Box::new(mountpoint::Mountpoint)),
        "swaplabel" => Some(Box::new(swaplabel::Swaplabel)),
        "swapoff" => Some(Box::new(swapoff::Swapoff)),
        "umount" => Some(Box::new(umount::Umount)),
        "utilbox" => Some(Box::new(utilbox::Utilbox)),
        _ => None,
    }
}

pub static COMMANDS: [&str; 7] = [
    "bootstrap",
    "clear",
    "mountpoint",
    "swaplabel",
    "swapoff",
    "umount",
    "utilbox",
];
