use clap::{ArgMatches, Command};
use shitbox::Cmd;

#[derive(Debug)]
pub struct Mount;

impl Cmd for Mount {
    fn cli(&self) -> Command {
        Command::new("mount")
    }

    fn run(&self, _matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
