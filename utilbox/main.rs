use std::process;

mod commands;

fn main() {
    if let Some(progname) = shitbox::progname() {
        if let Some(command) = commands::get(&progname) {
            let cli = command.cli();
            if let Err(e) = command.run(&cli.get_matches()) {
                eprintln!("{progname}: Error: {e}");
                process::exit(1);
            }
        } else {
            eprintln!("shitbox: Error: unknown command {progname}");
            process::exit(1);
        }
    }
}
