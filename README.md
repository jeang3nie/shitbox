Contents
========
* [Introduction](#introduction)
* [Provided Commands](#provided_commands)
* [Scope](#scope)
* [Installation](#installation)

## Introduction
*Shitbox* is inspired by the project [busybox](https://busybox.net/) but with a
much more limited scope. While Busybox aims to be "*The swiss army knife of
embedded linux*" you can think of shitbox as being more like "*The box store
multi tool of embedded linux*".

All joking aside the utilities which are present function mostly as expected and
the code aims to be robust. It's written in Rust, not C, for whatever that's
worth. Like Busybox it is a multi-call binary which is therefore able to share
code between applets, making for an overall smaller binary.

## Provided Commands
- b2sum
- base32
- base64
- basename
- chgrp
- chmod
- chown
- chroot
- clear
- cut
- df
- dirname
- echo
- false
- factor
- fold
- groups
- head
- hostid
- hostname
- link
- ln
- logname
- md5sum
- mkfifo
- mknod
- mktemp
- mountpoint
- nologin
- nproc
- printenv
- pwd
- readlink
- realpath
- rev
- rm
- rmdir
- sha1sum
- sha224sum
- sha256sum
- sha384sum
- sha512sum
- sleep
- swaplabel
- swapoff
- sync
- touch
- true
- truncate
- umount
- unlink
- wc
- which
- who
- whoami
- yes

## Scope
*Shitbox* does not aim to supply an entire system of utilities, but rather a
subset of the most common Unix shell utilities. Things which are out of scope
for the project include:
- Shells
- Network servers
- Kernel module handling utilities
- Anything requiring suid or sgid bits such as `su` or `sudo`
The code is not intended to be portable across different architectures, only
Linux is supported.

Full compatability with GNU coreutils is not intended. Compliance with POSIX is
the main goal, with extensions provided by GNU or BSD where they make sense, and
certain tweaks to improve consistency of behavior where they do not interfere
with POSIX compliance.

## Installation
Building is done using the official Rust toolchain. It is recommended that you
install your toolchain using Rustup rather than distro packages, as old compiler
versions are not supported.
```Sh
cargo build --release
```
This produces three binaries: corebox, hashbox and utilbox. Most of the commands
ordinarily provided by GNU coreutils are applets of `corebox`, with the exception
of cryptographic hash related commands which are split out into `hashbox`. Linux
swpecific commands ordinarily provided by the util-linux package are in `utilbox`.

The `bootstrap` applet provides facility for installing the binary, creating all
required symlinks and installing some nice to haves such as **Unix man pages**
and **shell completions** [see below].
```Sh
target/release/corebox help bootstrap
```
### Supported shells for completions
- Bash
- Fish
- NuShell
- PowerShell
- Zsh

