#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
use std::{env, path::PathBuf, string::ToString};

pub mod args;
mod cmd;
pub mod fs;
pub mod math;

pub use cmd::Cmd;

/// Defines the location relative to the binary where a command will be installed
#[derive(Debug, Clone, Copy)]
pub enum Path {
    /// /bin
    Bin,
    /// /sbin
    Sbin,
    /// /usr/bin
    UsrBin,
    /// /usr/sbin
    UsrSbin,
}

impl Path {
    #[must_use]
    pub fn to_str(&self, usr: bool) -> &'static str {
        match self {
            Self::UsrBin if usr => "usr/bin",
            Self::UsrSbin if usr => "usr/sbin",
            Self::Bin | Self::UsrBin => "bin",
            Self::Sbin | Self::UsrSbin => "sbin",
        }
    }
}

/// Returns the basename of the command as it was called
#[must_use]
pub fn progname() -> Option<String> {
    env::args()
        .next()
        .and_then(|x| {
            PathBuf::from(x)
                .file_name()
                .map(|x| x.to_str().map(ToString::to_string))
        })
        .flatten()
}

/// Returns the path to the binary, if called as "shitbox". Used in the bootstrap
/// applet to find the binary location in order to install it into it's permanent
/// location
#[must_use]
pub fn progpath() -> Option<PathBuf> {
    match progname() {
        Some(s) if s == "shitbox" => env::args().next().map(PathBuf::from),
        _ => None,
    }
}
