//! Math related functions not included in std
#[must_use]
pub fn is_prime(num: u64) -> bool {
    match num {
        0 | 1 => false,
        2 | 3 | 5 | 7 | 11 | 13 => true,
        x if x % 2 == 0 => false,
        _ => {
            let mut x: u64 = 2;
            while x < num / 2 {
                if num % x == 0 {
                    return false;
                }
                x += 1;
            }
            true
        }
    }
}
