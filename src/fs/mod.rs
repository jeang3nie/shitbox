use std::{
    fmt,
    fs::{self, DirEntry, Metadata},
    io,
};

#[derive(Clone, Copy, Debug)]
pub enum FileType {
    File,
    Dir,
    Symlink,
}

impl From<Metadata> for FileType {
    fn from(meta: Metadata) -> Self {
        let ft = meta.file_type();
        if ft.is_dir() {
            Self::Dir
        } else if ft.is_symlink() {
            Self::Symlink
        } else {
            Self::File
        }
    }
}

impl From<fs::FileType> for FileType {
    fn from(value: fs::FileType) -> Self {
        if value.is_dir() {
            Self::Dir
        } else if value.is_file() {
            Self::File
        } else {
            Self::Symlink
        }
    }
}

impl TryFrom<DirEntry> for FileType {
    type Error = io::Error;

    fn try_from(value: DirEntry) -> Result<Self, Self::Error> {
        let ft = value.file_type()?;
        Ok(ft.into())
    }
}

impl fmt::Display for FileType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::File => "file",
                Self::Symlink => "symlink",
                Self::Dir => "directory",
            }
        )
    }
}
