use clap::{Arg, ArgAction, Command};
use shitbox::Cmd;
use utmp_rs::UtmpEntry;

#[derive(Debug)]
pub struct Who;

impl Cmd for Who {
    fn cli(&self) -> Command {
        Command::new("who")
            .about("show who is logged on")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .arg(
                Arg::new("heading")
                    .help("print line of column headings")
                    .short('H')
                    .long("heading")
                    .action(ArgAction::SetTrue),
            )
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if matches.get_flag("heading") {
            println!("NAME     LINE         TIME");
        }
        let entries = utmp_rs::parse_from_path("/var/run/utmp")?;
        for e in entries.iter() {
            match e {
                UtmpEntry::UserProcess {
                    pid: _,
                    line,
                    user,
                    host: _,
                    session: _,
                    time,
                } => {
                    println!(
                        "{user:9}{line:13}{} {}:{}",
                        time.date(),
                        time.hour(),
                        time.minute()
                    );
                }
                _ => {}
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
