use super::Cmd;
use clap::{Arg, ArgAction, Command};
use std::{env, path::PathBuf};

#[derive(Debug)]
pub struct Pwd;

impl Cmd for Pwd {
    fn cli(&self) -> clap::Command {
        Command::new("pwd")
            .about("return working directory name")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("logical")
                    .short('L')
                    .long("logical")
                    .help("use PWD from environment, even if it contains symlinks")
                    .action(ArgAction::SetTrue),
                Arg::new("physical")
                    .short('P')
                    .long("physical")
                    .help("avoid all symlinks")
                    .conflicts_with("logical")
                    .action(ArgAction::SetTrue),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let cwd = if matches.get_flag("logical") {
            PathBuf::from(env::var("PWD")?)
        } else if matches.get_flag("physical") {
            env::current_dir()?.canonicalize()?
        } else {
            env::current_dir()?
        };
        println!("{}", cwd.display());
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
