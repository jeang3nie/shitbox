use super::Cmd;
use clap::Command;
use std::{error::Error, process};

#[derive(Debug)]
pub struct True;

impl Cmd for True {
    fn cli(&self) -> clap::Command {
        Command::new("true")
            .about("Does nothing successfully")
            .long_about("Exit with a status code indicating success")
            .author("Nathan Fisher")
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn Error>> {
        process::exit(0);
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
