use super::Cmd;
use clap::{value_parser, Arg, ArgAction, Command};
use data_encoding::BASE32;
use shitbox::args;
use std::{
    error::Error,
    fs,
    io::{self, Read, Write},
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

#[derive(Debug)]
pub struct Base32;

impl Cmd for Base32 {
    fn cli(&self) -> clap::Command {
        Command::new("base32")
            .author("Nathan Fisher")
            .about("Base32 encode/decode data and print to standard output")
            .args(args())
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let color = match matches.get_one::<String>("color").map(String::as_str) {
            Some("always") => ColorChoice::Always,
            Some("ansi") => ColorChoice::AlwaysAnsi,
            Some("auto") => {
                if atty::is(atty::Stream::Stdout) {
                    ColorChoice::Auto
                } else {
                    ColorChoice::Never
                }
            }
            _ => ColorChoice::Never,
        };
        if let Some(files) = matches.get_many::<String>("file") {
            let (len, _) = files.size_hint();
            for (index, file) in files.into_iter().enumerate() {
                if { len > 1 || matches.get_flag("verbose") } && !matches.get_flag("QUIET") {
                    let mut stdout = StandardStream::stdout(color);
                    stdout.set_color(ColorSpec::new().set_fg(Some(Color::Green)))?;
                    match index {
                        0 => writeln!(stdout, "===> {file} <==="),
                        _ => writeln!(stdout, "\n===> {file} <==="),
                    }?;
                    stdout.reset()?;
                } else if index > 0 {
                    println!();
                }
                let contents = get_contents(file)?;
                if matches.get_flag("DECODE") {
                    decode_base32(contents, matches.get_flag("IGNORE"))?;
                } else {
                    encode_base32(
                        &contents,
                        match matches.get_one::<usize>("WRAP") {
                            Some(c) => *c,
                            None => {
                                return Err(
                                    io::Error::new(io::ErrorKind::Other, "Invalid wrap").into()
                                )
                            }
                        },
                    );
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}

pub fn args() -> [Arg; 7] {
    [
        args::file(),
        Arg::new("DECODE")
            .help("Decode rather than encode")
            .short('d')
            .long("decode")
            .action(ArgAction::SetTrue),
        Arg::new("IGNORE")
            .help("Ignore whitespace when decoding")
            .short('i')
            .long("ignore-space")
            .action(ArgAction::SetTrue),
        Arg::new("WRAP")
            .help("Wrap encoded lines after n characters")
            .short('w')
            .long("wrap")
            .value_parser(value_parser!(usize))
            .default_value("76"),
        args::color(),
        args::verbose(),
        Arg::new("QUIET")
            .help("Do not display header, even with multiple files")
            .short('q')
            .long("quiet")
            .action(ArgAction::SetTrue),
    ]
}

fn decode_base32(mut contents: String, ignore: bool) -> Result<(), Box<dyn Error>> {
    if ignore {
        contents.retain(|c| !c.is_whitespace());
    } else {
        contents = contents.replace('\n', "");
    }
    let decoded = BASE32.decode(contents.as_bytes())?;
    let output = String::from_utf8(decoded)?;
    println!("{}\n", output.trim_end());
    Ok(())
}

fn encode_base32(contents: &str, wrap: usize) {
    BASE32
        .encode(contents.as_bytes())
        .chars()
        .collect::<Vec<char>>()
        .chunks(wrap)
        .map(|c| c.iter().collect::<String>())
        .for_each(|line| println!("{line}"));
}

fn get_contents(file: &str) -> Result<String, Box<dyn Error>> {
    let mut contents = String::new();
    if file == "-" {
        io::stdin().read_to_string(&mut contents)?;
    } else {
        contents = fs::read_to_string(file)?;
    }
    Ok(contents)
}
