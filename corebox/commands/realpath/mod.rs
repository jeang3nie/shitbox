use super::Cmd;
use clap::{Arg, ArgAction, Command, ValueHint};
use std::{env, fs, process};

#[derive(Debug)]
pub struct Realpath;

impl Cmd for Realpath {
    fn cli(&self) -> clap::Command {
        Command::new("realpath")
            .about("return resolved physical path")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("quiet")
                    .short('q')
                    .long("quiet")
                    .help("ignore warnings")
                    .action(ArgAction::SetTrue),
                Arg::new("path")
                    .value_hint(ValueHint::AnyPath)
                    .num_args(1..)
                    .required(false),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("path") {
            let mut erred = false;
            for f in files {
                match fs::canonicalize(f) {
                    Ok(p) => println!("{}", p.display()),
                    Err(e) => {
                        if !matches.get_flag("quiet") {
                            erred = true;
                        } else {
                            return Err(e.into());
                        }
                    }
                }
            }
            if erred {
                process::exit(1);
            }
        } else {
            match env::current_dir().and_then(fs::canonicalize) {
                Ok(p) => println!("{}", p.display()),
                Err(e) => {
                    if matches.get_flag("quiet") {
                        process::exit(1);
                    } else {
                        return Err(e.into());
                    }
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
