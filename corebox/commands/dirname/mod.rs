use super::Cmd;
use clap::{Arg, ArgAction, Command};
use std::path::Path;

#[derive(Debug)]
pub struct Dirname;

impl Cmd for Dirname {
    fn cli(&self) -> clap::Command {
        Command::new("dirname")
            .about("strip last component from file name")
            .args([
                Arg::new("zero")
                    .short('z')
                    .long("zero")
                    .help("end each output line with NUL, not newline")
                    .action(ArgAction::SetTrue),
                Arg::new("name").num_args(1..).required(true),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(names) = matches.get_many::<String>("name") {
            names.for_each(|name| {
                let path = match Path::new(name).parent() {
                    Some(p) => p,
                    None => Path::new("."),
                };
                let path = path.to_string_lossy();
                let path = if path.is_empty() {
                    String::from(".")
                } else {
                    path.to_string()
                };
                if matches.get_flag("zero") {
                    print!("{path}\0");
                } else {
                    println!("{path}");
                }
            });
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
