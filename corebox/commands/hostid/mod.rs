use super::Cmd;
use clap::Command;

#[derive(Debug)]
pub struct Hostid;

impl Cmd for Hostid {
    fn cli(&self) -> clap::Command {
        Command::new("hostid")
            .about("print the numeric identifier for the current host")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let hostid = unsafe { libc::gethostid() };
        let hostid: String = format!("{hostid:x}").chars().skip(8).collect();
        println!("{}", hostid);
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
