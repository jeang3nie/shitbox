use super::Cmd;
use clap::{Arg, ArgMatches, Command};

#[derive(Debug)]
pub struct Basename;

impl Cmd for Basename {
    fn cli(&self) -> clap::Command {
        Command::new("basename")
            .about("Print NAME with any leading directory components removed.")
            .long_about(
                "Print NAME with any leading directory components removed.\n\
                If specified, also remove a trailing SUFFIX.",
            )
            .author("Nathan Fisher")
            .args([
                Arg::new("NAME")
                    .index(1)
                    .required(true)
                    .help("the filename to process"),
                Arg::new("SUFFIX")
                    .index(2)
                    .required(false)
                    .help("the suffix to be removed"),
            ])
    }

    fn run(&self, matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(mut base) = matches
            .get_one::<String>("NAME")
            .and_then(|x| x.split('/').last())
        {
            if let Some(suffix) = matches.get_one::<String>("SUFFIX") {
                base = match base.strip_suffix(suffix) {
                    Some(b) => b,
                    None => base,
                };
            }
            println!("{base}");
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
