use super::Cmd;
use clap::{Arg, Command};
use std::{env, error::Error};

#[derive(Debug)]
pub struct Echo;

impl Cmd for Echo {
    fn cli(&self) -> clap::Command {
        Command::new("echo")
            .about("Display a line of text")
            .long_about("Echo the STRING(s) to standard output")
            .author("Nathan Fisher")
            .args([
                Arg::new("inline")
                    .short('n')
                    .help("Do not output a trailing newline"),
                Arg::new("STRING").num_args(1..),
            ])
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn Error>> {
        let args: Vec<String> = env::args().collect();
        let idx = match shitbox::progname() {
            Some(s) if s.as_str() == "echo" => 1,
            Some(_) => 2,
            None => unreachable!(),
        };
        let len = args.len();
        let n = len > idx && args[idx] == "-n";
        let i = if n { idx + 1 } else { idx };
        for (index, arg) in args.iter().enumerate().skip(i) {
            if index < len - 1 {
                print!("{arg} ");
            } else {
                print!("{arg}");
            }
        }
        if !n {
            println!();
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
