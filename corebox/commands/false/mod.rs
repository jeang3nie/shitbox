use super::Cmd;
use clap::Command;
use std::{error::Error, process};

#[derive(Debug)]
pub struct False;

impl Cmd for False {
    fn cli(&self) -> clap::Command {
        Command::new("false")
            .about("Does nothing unsuccessfully")
            .long_about("Exit with a status code indicating failure")
            .author("Nathan Fisher")
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn Error>> {
        process::exit(1);
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
