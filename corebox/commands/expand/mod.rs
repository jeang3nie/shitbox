use super::Cmd;
use clap::{Arg, ArgAction, Command, ValueHint};

#[derive(Debug)]
pub struct Expand;

impl Cmd for Expand {
    fn cli(&self) -> clap::Command {
        Command::new("expand")
            .about("convert tabs to spaces")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("file")
                    .num_args(1..)
                    .default_value("-")
                    .value_name("FILE")
                    .value_hint(ValueHint::FilePath),
                Arg::new("initial")
                    .short('i')
                    .long("initial")
                    .help("do not convert tabs after non blanks")
                    .action(ArgAction::SetTrue),
                Arg::new("tabs")
                    .short('t')
                    .long("tabs")
                    .default_value("8")
                    .help(
                        "Specify the tab stops. The application shall ensure that \
                        the argument tablist consists of either a single positive \
                        decimal integer or a list of tabstops. If a single number \
                        is given, tabs shall be set that number of column positions \
                        apart instead of the default 8.",
                    )
                    .value_name("tablist")
                    .value_delimiter(',')
                    .num_args(1..),
            ])
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
