use super::Cmd;
use clap::{Arg, Command};
use shitbox::args;
use std::{fs, io};

#[derive(Debug)]
pub struct Link;

impl Cmd for Link {
    fn cli(&self) -> clap::Command {
        Command::new("link")
            .about("call the link function to create a link to a file")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("file1").required(true).index(1),
                Arg::new("file2").required(true).index(2),
                args::verbose(),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let Some(f1) = matches.get_one::<String>("file1") else {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                "missing file 1",
            )))
        };
        let Some(f2) = matches.get_one::<String>("file2") else {
            return Err(Box::new(io::Error::new(
                io::ErrorKind::Other,
                "missing file 2",
            )))
        };
        fs::hard_link(f1, f2)?;
        if matches.get_flag("verbose") {
            println!("{f2} -> {f1}");
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
