use super::Cmd;
use clap::{Arg, Command};

#[derive(Debug)]
pub struct Yes;

impl Cmd for Yes {
    fn cli(&self) -> clap::Command {
        Command::new("yes")
            .about("output a string repeatedly until killed")
            .author("Nathan Fisher")
            .arg(Arg::new("msg").num_args(1).default_value("y"))
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let msg = matches.get_one::<String>("msg").unwrap();
        loop {
            println!("{msg}");
        }
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
