use super::Cmd;
use clap::{Arg, ArgMatches, Command};
use mode::Parser;
use shitbox::args;
use std::error::Error;

#[derive(Debug)]
pub struct MkFifo;

impl Cmd for MkFifo {
    fn cli(&self) -> clap::Command {
        Command::new("mkfifo")
            .about("make FIFO special files")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("mode")
                    .help(
                        "Set  the  file  permission  bits of the newly-created \
                        FIFO to the specified mode value.",
                    )
                    .long_help(
                        "Set  the  file  permission  bits of the newly-created \
                        FIFO to the specified mode value. The mode option-argument \
                        shall be the same as the mode operand defined for the \
                        chmod  utility. In the symbolic_mode strings, the op \
                        characters '+' and '-' shall be interpreted relative to \
                        an assumed initial mode of a=rw.",
                    )
                    .short('m')
                    .long("mode")
                    .value_name("MODE")
                    .num_args(1),
                args::verbose(),
                Arg::new("file").num_args(1..).required(true),
            ])
    }

    fn run(&self, matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
        let mode = if let Some(m) = matches.get_one::<String>("mode") {
            Parser::new(0o666).parse(m)?
        } else {
            0o666
        };
        if let Some(files) = matches.get_many::<String>("file") {
            for f in files {
                stat::mkfifo(f.as_str(), mode)?;
                if matches.get_flag("verbose") {
                    println!(
                        "{}: created pipe '{f}' with mode {mode:o}",
                        self.cli().get_name()
                    );
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
