use errno::Errno;
use sc::syscall;
use std::{error::Error, ffi::CString, fs::File, mem, os::fd::AsRawFd};

pub fn mknod(path: &str, mode: u32, dev: u64) -> Result<(), Box<dyn Error>> {
    let ret = unsafe {
        syscall!(
            MKNODAT,
            libc::AT_FDCWD,
            CString::new(path)?.as_ptr(),
            mode,
            dev
        )
    };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}

pub fn mkfifo(path: &str, mode: u32) -> Result<(), Box<dyn Error>> {
    let ret = unsafe {
        syscall!(
            MKNODAT,
            libc::AT_FDCWD,
            CString::new(path)?.as_ptr(),
            mode | libc::S_IFIFO,
            0
        )
    };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}

pub fn statfs(dir: &str) -> Result<libc::statvfs, Box<dyn Error>> {
    let mut buf = mem::MaybeUninit::<libc::statvfs>::uninit();
    let fd = File::open(dir)?;
    let ret = unsafe { libc::fstatvfs(fd.as_raw_fd(), buf.as_mut_ptr()) };
    if ret == 0 {
        let buf = unsafe { buf.assume_init() };
        Ok(buf)
    } else {
        let e = unsafe { *libc::__errno_location() };
        Err(Errno::from(i64::from(e)).into())
    }
}
