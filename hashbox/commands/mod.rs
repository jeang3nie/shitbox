use shitbox::Cmd;

mod b2sum;
mod bootstrap;
mod hashbox;
mod md5sum;
mod sha1sum;
mod sha224sum;
mod sha256sum;
mod sha384sum;
mod sha512sum;

/// Parses a string into a command to run
#[must_use]
pub fn get(name: &str) -> Option<Box<dyn Cmd>> {
    match name {
        "b2sum" => Some(Box::new(b2sum::B2sum)),
        "bootstrap" => Some(Box::new(bootstrap::Bootstrap)),
        "hashbox" => Some(Box::new(hashbox::Hashbox)),
        "md5sum" => Some(Box::new(md5sum::Md5sum)),
        "sha1sum" => Some(Box::new(sha1sum::Sha1sum)),
        "sha224sum" => Some(Box::new(sha224sum::Sha224sum)),
        "sha256sum" => Some(Box::new(sha256sum::Sha256sum)),
        "sha384sum" => Some(Box::new(sha384sum::Sha384sum)),
        "sha512sum" => Some(Box::new(sha512sum::Sha512sum)),
        _ => None,
    }
}

pub static COMMANDS: [&str; 8] = [
    "b2sum",
    "bootstrap",
    "md5sum",
    "sha1sum",
    "sha224sum",
    "sha256sum",
    "sha384sum",
    "sha512sum",
];
