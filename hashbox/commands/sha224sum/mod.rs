use super::Cmd;
use crate::hash::{self, HashType};
use clap::Command;
use sha2::{Digest, Sha224};
use shitbox::args;
use std::io;

#[derive(Debug, Default)]
pub struct Sha224sum;

impl Cmd for Sha224sum {
    fn cli(&self) -> clap::Command {
        Command::new("sha224sum")
            .about("compute and check SHA1 message digest")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([args::check(), args::file()])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("file") {
            let mut erred = 0;
            for f in files {
                if matches.get_flag("check") {
                    if f == "-" {
                        return Err(
                            io::Error::new(io::ErrorKind::Other, "no file specified").into()
                        );
                    }
                    hash::check_sums(f, HashType::Sha224, &mut erred)?;
                } else {
                    let hasher = Sha224::new();
                    let s = hash::compute_hash(f, hasher)?;
                    println!("{s}  {f}");
                }
            }
            if erred > 0 {
                let msg = format!("WARNING: {erred} computed checksums did NOT match");
                return Err(io::Error::new(io::ErrorKind::Other, msg).into());
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
