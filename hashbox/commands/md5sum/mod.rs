use crate::hash::{self, HashType};
use clap::Command;
use md5::{Digest, Md5};
use shitbox::args;
use shitbox::Cmd;
use std::{io, process};

#[derive(Debug, Default)]
pub struct Md5sum;

impl Cmd for Md5sum {
    fn cli(&self) -> clap::Command {
        Command::new("md5sum")
            .about("compute and check MD5 message digest")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([args::check(), args::file()])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("file") {
            let mut erred = 0;
            for f in files {
                if matches.get_flag("check") {
                    if f == "-" {
                        return Err(
                            io::Error::new(io::ErrorKind::Other, "no file specified").into()
                        );
                    }
                    hash::check_sums(f, HashType::Md5, &mut erred)?;
                } else {
                    let hasher = Md5::new();
                    let s = hash::compute_hash(f, hasher)?;
                    println!("{s}  {f}");
                }
            }
            if erred > 0 {
                println!("md5sum: WARNING: {erred} computed checksum did NOT match");
                process::exit(1);
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
