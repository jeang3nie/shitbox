#![warn(clippy::all, clippy::pedantic)]
#![allow(clippy::missing_errors_doc)]
use errno::Errno;
use sc::syscall;
use std::{error::Error, ffi::CString};

mod mntent;
mod mntentries;
mod mntflags;

pub use self::{
    mntent::MntEntry,
    mntentries::MntEntries,
    mntflags::{Flags as MntFlags, ParseFlagsError},
};

/// Wraps the Linux `SYS_mount` syscall in a nicer interface
pub fn mount(
    dev: &str,
    mountpoint: &str,
    fstype: &str,
    flags: u32,
    opts: Option<&str>,
) -> Result<(), Box<dyn Error>> {
    let ret = unsafe {
        if let Some(opts) = opts {
            syscall!(
                MOUNT,
                CString::new(dev)?.as_ptr(),
                CString::new(mountpoint)?.as_ptr(),
                CString::new(fstype)?.as_ptr(),
                flags as usize,
                CString::new(opts)?.as_ptr()
            )
        } else {
            syscall!(
                MOUNT,
                CString::new(dev)?.as_ptr(),
                CString::new(mountpoint)?.as_ptr(),
                CString::new(fstype)?.as_ptr(),
                flags as usize
            )
        }
    };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}

pub fn swapon(dev: &str, flags: usize) -> Result<(), Box<dyn Error>> {
    let ret = unsafe { syscall!(SWAPON, CString::new(dev)?.as_ptr(), flags) };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}

pub fn swapoff(path: &str) -> Result<(), Box<dyn Error>> {
    let ret = unsafe { syscall!(SWAPOFF, CString::new(path)?.as_ptr()) };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}

pub fn umount(special: &str, flags: u32) -> Result<(), Box<dyn Error>> {
    let ret = unsafe { syscall!(UMOUNT2, CString::new(special)?.as_ptr(), flags) };
    if ret == 0 {
        Ok(())
    } else {
        Err(Errno::from(ret).into())
    }
}
