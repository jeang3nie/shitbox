use std::ops::{BitAnd, BitAndAssign, BitOr, BitOrAssign};

#[derive(PartialEq)]
/// The granularity of the given permissions
pub enum Who {
    /// applies for the current user
    User = 0o100,
    /// applies for the current group
    Group = 0o10,
    /// applies for everyone else
    Other = 0o1,
}

impl BitAnd<Who> for u32 {
    type Output = u32;

    fn bitand(self, rhs: Who) -> Self::Output {
        self & rhs as u32
    }
}

impl BitAnd<u32> for Who {
    type Output = u32;

    fn bitand(self, rhs: u32) -> Self::Output {
        self as u32 & rhs
    }
}

impl BitAndAssign<Who> for u32 {
    fn bitand_assign(&mut self, rhs: Who) {
        *self = *self & rhs;
    }
}

impl BitOr<Who> for u32 {
    type Output = u32;

    fn bitor(self, rhs: Who) -> Self::Output {
        self | rhs as u32
    }
}

impl BitOrAssign<Who> for u32 {
    fn bitor_assign(&mut self, rhs: Who) {
        *self = *self | rhs;
    }
}
